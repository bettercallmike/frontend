import { Overview } from './../../model/Overview';
import { Component, OnInit } from '@angular/core';
import { NzTableQueryParams, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import * as isUndefined from 'lodash';
import * as cloneDeep from 'lodash';
import { HttpRequestsService } from 'src/app/services/http-requests.service';
import { tap } from 'lodash';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  overviews:any = [];

  pageIndex: any;
  pageSize: any;
  total: any;
  preventCall_OnQueryParams: boolean=false;
  regionNames:any=[];
  
  isLoading: boolean= false;


  constructor(private httpService:HttpRequestsService) { }

  ngOnInit(page?: any): void {
    
    this.pageSize = 2;
    this.isLoading = true;
    this.pageIndex = 1;
          
    if ( !(isUndefined(page)) ) {
      this.pageIndex = page;
     }
     const pageNum = this.pageIndex - 1;
     
     this.httpService
     .getRequestOverview(pageNum, this.pageSize)
     .pipe()
     .subscribe( data => {   
                           this.overviews = data.overviewDTOList;
                           this.total = data.count;
                        },
                err =>  { 
                            console.log(err);
                        },
                () =>   {      
                             
                              this.regionNames=[];  
                              
                              this.overviews.forEach((value: string[], key: string) => {

                                        console.log("value is " + JSON.stringify(value),"key"+ key);
                            });

                              // this.regionNames =  this.overviews.regionNameWithCountries.keys();

                               this.isLoading=false;
                               console.log('isLoading: ', this.isLoading);
                        });

  }


  async getCurrentPage(index: any) {
    console.log("gets called current page");
    this.pageIndex = index;
    this.preventCall_OnQueryParams = true;
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    //** prevent execution */
    if (this.preventCall_OnQueryParams) {
      const { pageSize, pageIndex, sort } = params;
      this.pageIndex = pageIndex;
      this.getPageData(pageIndex);
      return;
    }
  }


  getPageData(index?: any) {
    let overviewArr: any[];

    let pageIndex = 0;

    if(isUndefined(index)){  pageIndex = (this.pageIndex - 1);  }
    else{
           pageIndex = (index - 1);
           this.pageIndex = index;
       }

    this.httpService
        .getRequestOverview(pageIndex, this.pageSize)
        .subscribe(
                   data => {
                    overviewArr = data.overviewDTOList;
                              this.total = data.count;
                           },
                   err =>  { 
                               console.log(err);
                           },
                   () =>   { 
                              this.overviews = [];
                              this.overviews = cloneDeep(overviewArr);
                        
                           });

  }


}

