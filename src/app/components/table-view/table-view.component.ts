import { HttpRequestsService } from './../../services/http-requests.service';
import { Component, OnInit } from '@angular/core';
import * as isUndefined from 'lodash';
import * as cloneDeep from 'lodash';

import { NzTableFilterFn, NzTableFilterList, NzTableQueryParams, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { Country } from 'src/app/model/Country';


@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css']
})
export class TableViewComponent implements OnInit {
  
  countries:any = [];

  pageIndex: any;
  pageSize: any;
  total: any;
  preventCall_OnQueryParams: boolean=false;

  isLoading: boolean= false;


  listOfColumns: ColumnItem[] = [
    {
      name: 'Name',
      sortOrder: null,
      sortFn: (a: Country, b: Country) => a.name.localeCompare(b.name),
     
    },
    {
      name: 'coutry_code_2',
      sortOrder: null,
      sortFn: (a: Country, b: Country) => a.area.localeCompare(b.area),
     
    },
    {
      name: 'area',
      sortOrder: null,
      sortFn: (a: Country, b: Country) => a.countryCode2.localeCompare(b.countryCode2),
     
    }

  ]



  constructor(private httpService:HttpRequestsService ) { }

  ngOnInit(page?: any): void {
     
    this.pageSize = 5;
    this.isLoading = true;
    this.pageIndex = 1;
  
    
     
    if ( !(isUndefined(page)) ) {
      this.pageIndex = page;
     }
     const pageNum = this.pageIndex - 1;

     this.httpService
     .getRequestCountries(pageNum, this.pageSize)
     .subscribe( data => {
                           this.countries = data.countries;
                           this.total = data.count;
                        },
                err =>  { 
                            console.log(err);
                        },
                () =>   {      
                              this.countries.forEach((c: any) => {
                                   console.log( "country is :"+ JSON.stringify(c) );
                              });
                          
                            this.isLoading=false;
                            console.log('isLoading: ', this.isLoading);
                        });

  }
  

  trackByName(_: number, item: ColumnItem): string {
    return item.name;
  }


  async getCurrentPage(index: any) {
    console.log("gets called current page");
    this.pageIndex = index;
    this.preventCall_OnQueryParams = true;
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    //** prevent execution */
    if (this.preventCall_OnQueryParams) {
      const { pageSize, pageIndex, sort } = params;
      this.pageIndex = pageIndex;
      this.getPageData(pageIndex);
      return;
    }
  }


  sortByAge(): void {
    console.log("sort by age gets called");
    this.countries
      .forEach( (item: { name: string; sortOrder: string | null; }) => {
                if (item.name === 'Name') {
                  item.sortOrder = 'descend';
                  console.log("name equals" + item.name);
                } 
                else {
                  item.sortOrder = null;
                }
      
    });
  }


  getPageData(index?: any) {
    let countryArr: isUndefined.List<any> | null | undefined = [];

    let pageIndex = 0;

    if(isUndefined(index)){  pageIndex = (this.pageIndex - 1);  }
    else{
           pageIndex = (index - 1);
           this.pageIndex = index;
       }

    this.httpService
        .getRequestCountries(pageIndex, this.pageSize)
        .subscribe(
                   data => {
                              countryArr = data.countries;
                              this.total = data.count;
                           },
                   err =>  { 
                               console.log(err);
                           },
                   () =>   { 
                              this.countries = [];
                              this.countries = cloneDeep(countryArr);
                        
                           });

  }


}

interface ColumnItem {
  name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn<Country> | null;
}