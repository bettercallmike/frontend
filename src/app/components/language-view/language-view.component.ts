import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpRequestsService } from 'src/app/services/http-requests.service';

@Component({
  selector: 'app-language-view',
  templateUrl: './language-view.component.html',
  styleUrls: ['./language-view.component.css']
})
export class LanguageViewComponent implements OnInit {

  languages:any = [];
  countryName:any;
  gridStyle = {
    textAlign: 'center'
  };

  constructor(private httpService:HttpRequestsService,private _route: ActivatedRoute) { }

  ngOnInit(): void {

    this._route.params.subscribe(params => {
      this.countryName = params['name'];
      console.log("countryname is"  +this.countryName );
  });

  this.httpService
  .getRequestLanguagesOfCountry(this.countryName)
  .subscribe( data => {
                        this.languages = data;
                     },
             err =>  { 
                         console.log(err);
                     },
             () =>   {      
                          //  this.languages.forEach((l: any) => {
                          //       console.log( "language is :"+ JSON.stringify(;) );
                          //  });
                      
                         
                     });





  }

}
