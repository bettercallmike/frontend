import { Overview } from './../model/Overview';
import { Country } from 'src/app/model/Country';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CountriesDTO } from '../model/CountriesDTO';
import { LanguageDTO } from '../model/LanguageDTO';
import { OverviewDTO } from '../model/OverviewDTO';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestsService {
 
  urlCountry:String;
  urlLanguageOfCountry:String;
  urlOverview:String;

  constructor(private http: HttpClient) { 

      this.urlCountry='http://localhost:8088/api/countries';
      this.urlLanguageOfCountry='http://localhost:8088/api/countries/language'
      this.urlOverview='http://localhost:8088/api/overview';

  }


  getRequestCountries(pageIndex: number, pageSize: number): Observable<CountriesDTO> {

    return this.http.get<CountriesDTO>(this.urlCountry + '/' + pageIndex + '/' + pageSize);

  }


  getRequestLanguagesOfCountry(countryName: string): Observable<LanguageDTO> {

    return this.http.get<LanguageDTO>(this.urlLanguageOfCountry + '/' + countryName );

  }


  getRequestOverview(pageIndex: number, pageSize: number): Observable<OverviewDTO> {

    return this.http.get<OverviewDTO>(this.urlOverview + '/'  + pageIndex + '/' + pageSize);

  }


}
