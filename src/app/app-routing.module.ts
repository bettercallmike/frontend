import { TableViewComponent } from './components/table-view/table-view.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LanguageViewComponent } from './components/language-view/language-view.component';
import { OverviewComponent } from './components/overview/overview.component';

const routes: Routes = [
    { path: '',component: TableViewComponent},
    { path: 'countries', component: TableViewComponent },
    { path: 'overview', component: OverviewComponent },
    { path: 'language/:name', component: LanguageViewComponent}

];
// //   imports: [RouterModule.forChild(routes)],

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
